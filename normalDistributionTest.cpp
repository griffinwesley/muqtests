#include <MUQ/Approximation/PolynomialChaos/AdaptiveSmolyakPCE.h>
#include <MUQ/Approximation/PolynomialChaos/PCEFactory.h>
#include <MUQ/Approximation/Polynomials/Jacobi.h>
#include <MUQ/Approximation/Polynomials/Laguerre.h>
#include <MUQ/Approximation/Polynomials/Legendre.h>
#include <MUQ/Approximation/Polynomials/PhysicistHermite.h>
#include <MUQ/Approximation/Polynomials/ProbabilistHermite.h>
#include <MUQ/Approximation/Quadrature/GaussPattersonQuadrature.h>
#include <MUQ/Utilities/MultiIndices/MultiIndexFactory.h>

#include <algorithm>
#include <iostream>
#include <optional>
#include <random>
#include <string>

class BimodalModPiece : public muq::Modeling::ModPiece
{
public:
  static constexpr int const kInputDimensions = 3;
  static constexpr int const kOutputDimensions = 1;

  BimodalModPiece( double outputMean, double outputStdDev )
  : muq::Modeling::ModPiece{ kInputDimensions * Eigen::VectorXi::Ones( 1 ), 1 * Eigen::VectorXi::Ones( 1 ) },
    mRng{ mRd() },
    mOutputDistribution{ outputMean, outputStdDev }
  {}

  virtual ~BimodalModPiece()
  {}

protected:
  void EvaluateImpl( const muq::Modeling::ref_vector< Eigen::VectorXd >& inputs [[ maybe_unused ]] ) override
  {
    outputs.resize( 1 );
    outputs.at( 0 ).resize( kOutputDimensions );
    outputs.at( 0 )( 0 ) = mOutputDistribution( mRng );
  }

private:
  std::random_device mRd;
  std::mt19937 mRng;
  std::normal_distribution<> mOutputDistribution;
};

constexpr int const BimodalModPiece::kInputDimensions;
constexpr int const BimodalModPiece::kOutputDimensions;

int main( int argc, char* argv[] ) {
  if ( argc < 4 || ( argc > 4 && argc < 6 ) ) {
    std::cerr << "usage: " << argv[ 0 ] << " maxEvals outputMean outputStdDev [ jacobiA jacobi B ]\n";
    return 1;
  }

  int maxEvals = std::stoi( argv[ 1 ] );
  double outputMean = std::stod( argv[ 2 ] );
  double outputStdDev = std::stod( argv[ 3 ] );

  std::optional< double > jacobiA;
  std::optional< double > jacobiB;
  if ( argc > 5 ) {
    jacobiA = std::stod( argv [ 4 ] );
    jacobiB = std::stod( argv [ 5 ] );
  }

  std::vector< std::shared_ptr< muq::Approximation::IndexedScalarBasis > > polynomials(
      BimodalModPiece::kInputDimensions );
  std::generate( polynomials.begin(), polynomials.end(), [ & ]() {
    std::shared_ptr< muq::Approximation::IndexedScalarBasis > basis;

    if ( jacobiA && jacobiB ) {
      basis.reset( new muq::Approximation::Jacobi{ *jacobiA, *jacobiB } );
    } else {
      basis.reset( new muq::Approximation::Legendre );
    }

    return basis;
  } );

  std::vector< std::shared_ptr< muq::Approximation::Quadrature > > quadrature( BimodalModPiece::kInputDimensions );
  std::generate( quadrature.begin(), quadrature.end(), []() {
    return std::make_shared< muq::Approximation::GaussPattersonQuadrature >();
  } );

  const int initialOrder = 1;
  auto multis = muq::Utilities::MultiIndexFactory::CreateTotalOrder( BimodalModPiece::kInputDimensions, initialOrder );

  boost::property_tree::ptree options;
  options.put( "ShouldAdapt", 1 );
  options.put( "ErrorTol", 1e-8 );
  options.put( "MaximumEvals", maxEvals );

  auto model = std::make_shared< BimodalModPiece >( outputMean, outputStdDev );
  auto estimator = std::make_shared< muq::Approximation::AdaptiveSmolyakPCE>( model, quadrature, polynomials );
  auto expansion = estimator->Compute( multis, options );

  auto mean = expansion->Mean();
  auto variance = expansion->Variance();
  auto magnitude = expansion->Magnitude();

  std::cout << "Basis    : " << ( jacobiA && jacobiB ? "Jacobi" : "Legendre" ) << "\n";
  std::cout << "Error    : " << estimator->Error() << "\n";
  std::cout << "NumEvals : " << estimator->NumEvals() << "\n";
  std::cout << "Mean     : " << mean << "\n";
  std::cout << "Variance : " << variance << "\n";
  std::cout << "StdDev   : " << std::sqrt( variance[ 0 ] ) << "\n";
  std::cout << "Magnitude: " << magnitude << "\n";

  return 0;
}

