#include <MUQ/Approximation/PolynomialChaos/AdaptiveSmolyakPCE.h>
#include <MUQ/Approximation/PolynomialChaos/PCEFactory.h>
#include <MUQ/Approximation/Polynomials/Legendre.h>
#include <MUQ/Approximation/Polynomials/Jacobi.h>
#include <MUQ/Approximation/Quadrature/GaussPattersonQuadrature.h>
#include <MUQ/Utilities/MultiIndices/MultiIndexFactory.h>

#include <algorithm>
#include <iostream>
#include <random>
#include <string>

#include "pcg_random.hpp"

class BimodalModPiece : public muq::Modeling::ModPiece
{
public:
  static constexpr int const kInputDimensions = 3;
  static constexpr int const kOutputDimensions = 1;

  BimodalModPiece( double chanceOfZero, double outputMean, double outputStdDev )
  : muq::Modeling::ModPiece{ kInputDimensions * Eigen::VectorXi::Ones( 1 ), 1 * Eigen::VectorXi::Ones( 1 ) },
    mChanceOfZero{ chanceOfZero },
    mRng{ pcg_extras::seed_seq_from< std::random_device >() },
    mProbability{ 0 },
    mOutputDistribution{ outputMean, outputStdDev }
  {}

  virtual ~BimodalModPiece()
  {}

protected:
  void EvaluateImpl( const muq::Modeling::ref_vector< Eigen::VectorXd >& inputs [[ maybe_unused ]] ) override
  {
    outputs.resize( 1 );
    outputs.at( 0 ).resize( kOutputDimensions );
    outputs.at( 0 )( 0 ) = mProbability( mRng ) < mChanceOfZero ? 0.0 : mOutputDistribution( mRng );
  }

private:
  double mChanceOfZero;
  pcg32 mRng;
  std::uniform_real_distribution<> mProbability;
  std::normal_distribution<> mOutputDistribution;
};

constexpr int const BimodalModPiece::kInputDimensions;
constexpr int const BimodalModPiece::kOutputDimensions;

int main( int argc, char* argv[] ) {
  if ( argc < 3 || ( argc > 3 && argc < 5 ) || ( argc > 5 && argc < 7 ) ) {
    std::cerr << "usage: " << argv[ 0 ] << " maxEvals chanceOfZero [ outputMean outputStdDev [ jacobiA jacobi B ] ]\n";
    return 1;
  }

  int maxEvals = std::stoi( argv[ 1 ] );
  double chanceOfZero = std::stod( argv[ 2 ] );

  double outputMean = 350.0;
  double outputStdDev = 10.0;
  if ( argc > 3 ) {
    outputMean = std::stod( argv[ 3 ] );
    outputStdDev = std::stod( argv[ 4 ] );
  }

  double jacobiA = 0.0;
  double jacobiB = 0.0;
  if ( argc > 5 ) {
    jacobiA = std::stod( argv [ 5 ] );
    jacobiB = std::stod( argv [ 6 ] );
  }

  std::vector< std::shared_ptr< muq::Approximation::IndexedScalarBasis > > polynomials(
      BimodalModPiece::kInputDimensions );
  std::generate( polynomials.begin(), polynomials.end(), [ & ]() {
    return std::make_shared< muq::Approximation::Jacobi >( jacobiA, jacobiB );
  } );

  std::vector< std::shared_ptr< muq::Approximation::Quadrature > > quadrature( BimodalModPiece::kInputDimensions );
  std::generate( quadrature.begin(), quadrature.end(), []() {
    return std::make_shared< muq::Approximation::GaussPattersonQuadrature >();
  } );

  const int initialOrder = 1;
  auto multis = muq::Utilities::MultiIndexFactory::CreateTotalOrder( BimodalModPiece::kInputDimensions, initialOrder );

  boost::property_tree::ptree options;
  options.put( "ShouldAdapt", 1 );
  options.put( "ErrorTol", 1e-8 );
  options.put( "MaximumEvals", maxEvals );

  auto model = std::make_shared< BimodalModPiece >( chanceOfZero, outputMean, outputStdDev );
  auto estimator = std::make_shared< muq::Approximation::AdaptiveSmolyakPCE>( model, quadrature, polynomials );
  auto expansion = estimator->Compute( multis, options );

  auto mean = expansion->Mean();
  auto variance = expansion->Variance();
  auto magnitude = expansion->Magnitude();

  std::cout << "Error    : " << estimator->Error() << "\n";
  std::cout << "NumEvals : " << estimator->NumEvals() << "\n";
  std::cout << "Mean     : " << mean << "\n";
  std::cout << "Variance : " << variance << "\n";
  std::cout << "StdDev   : " << std::sqrt( variance[ 0 ] ) << "\n";
  std::cout << "Magnitude: " << magnitude << "\n";

  return 0;
}

